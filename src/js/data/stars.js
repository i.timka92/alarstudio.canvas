export default [
    {
        center: [150, 100],
        size: [90, 45],
        color: 'red',
        path2d: null,
    },
    {
        center: [450, 100],
        size: [90, 45],
        color: 'blue',
        path2d: null,
    },
    {
        center: [150, 300],
        size: [90, 45],
        color: 'green',
        path2d: null,
    },
    {
        center: [450, 300],
        size: [90, 45],
        color: 'yellow',
        path2d: null,
    },
    {
        center: [300, 500],
        size: [90, 55],
        color: 'black',
        path2d: null,
    }
];