import {getPointCoordsByAngleAndRadius} from "./math.logic";

function drawStarsOnCanvas(selector, stars) {
    let canvas = document.querySelector('canvas.stars'),
        context = canvas.getContext('2d');

    for (let star of stars) {
        let [cX, cY] = star.center;
        let [bigR, smallR] = star.size;

        star.path2d = drawStar(context, cX, cY, bigR, smallR, 5, star.color);
    }
}

function fillCanvas(selector, color) {
    let canvas = document.querySelector(selector),
        context = canvas.getContext('2d');

    context.fillStyle = color;
    context.fillRect(0, 0, canvas.width, canvas.height);
}

function drawStar(context, cX, cY, bigR, smallR, spikes, color) {
    let angleStep = 360 / spikes;

    let currentBigRAngle = -90,
        currentSmallRAngle = -90 + angleStep / 2;

    let path = new Path2D();
    for (let j = 0; j < spikes; j++) {
        let x, y;
        [x, y] = getPointCoordsByAngleAndRadius(currentBigRAngle, bigR);
        path.lineTo(x + cX, y + cY);

        [x, y] = getPointCoordsByAngleAndRadius(currentSmallRAngle, smallR);
        path.lineTo(x + cX, y + cY);

        currentBigRAngle += angleStep;
        currentSmallRAngle += angleStep;
    }
    path.closePath();
    context.fillStyle = color;
    context.fill(path);

    return path;
}


export {
    drawStarsOnCanvas,
    fillCanvas
}
