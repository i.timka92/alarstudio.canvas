function getPointCoordsByAngleAndRadius(angle, radius) {
    let radAngle = toRad(angle);
    return [
        Math.cos(radAngle) * radius,
        Math.sin(radAngle) * radius,
    ];
}

function toRad(angle) {
    return angle * Math.PI / 180;
}

export {
    getPointCoordsByAngleAndRadius
}