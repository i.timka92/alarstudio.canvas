import starsData from "./data/stars.js";
import {drawStarsOnCanvas, fillCanvas} from "./helpers/drawer";

let canvasStars = document.querySelector('canvas.stars');

drawStarsOnCanvas('canvas.stars', starsData);
canvasStars.addEventListener('click', onCanvasClick);

function onCanvasClick(event) {
    let canvas = event.target,
        context = canvas.getContext('2d');

    let color = 'white';
    for (let star of starsData) {
        if (context.isPointInPath(star.path2d, event.offsetX, event.offsetY)) {
            color = star.color;
            break;
        }
    }

    fillCanvas('canvas.background', color);
}